#include "flash_blob.h"

static const uint32_t flash_code[] =
{
    0XE00ABE00,0X062D780D,0X24084068,0XD3000040,0X1E644058,0X1C49D1FA,0X2A001E52,0X4770D1F2,
    0XD0012A01,0XD1172A02,0X6981483B,0X0212220F,0X61814311,0X60C14939,0X60C14939,0X61014939,
    0X61014939,0X02C069C0,0X4839D406,0X60014937,0X60412106,0X60814937,0X47702000,0XD0012801,
    0XD1082802,0X6841482C,0X43112202,0X68416041,0X43112201,0X20006041,0XB5304770,0X684A4926,
    0X4322154C,0X684A604A,0X432A2508,0X2200604A,0X48296002,0XE0004A26,0X698B6010,0XD1FB07DB,
    0X43A06848,0X68486048,0X604843A8,0XBD302000,0X47702001,0X4C18B5F0,0X15252300,0X313F2608,
    0X468C0989,0X6861E024,0X60614329,0X43316861,0X21406061,0XC080CA80,0X29001F09,0X4916D1FA,
    0X07FF69A7,0X4F12D002,0XE7F96039,0X050969A1,0XD0060F09,0X210F69A0,0X43080209,0X200161A0,
    0X6861BDF0,0X606143A9,0X43B16861,0X1C5B6061,0XD8D8459C,0XBDF02000,0X40022000,0X89ABCDEF,
    0X02030405,0X8C9DAEBF,0X13141516,0X00005555,0X40003000,0X00000FFF,0X0000AAAA,0X00000000,
};

const program_target_t flash_algo_STM32L0XX =
{
    0X20000021,  // Init
    0X2000005D,  // UnInit
    0X20000020,  // EraseChip
    0X2000007B,  // EraseSector
    0X200000B5,  // ProgramPage

    // BKPT : start of blob + 1
    // RSB  : address to access global/static data
    // RSP  : stack pointer
    {
        0X20000001,
        0X20000C00,
        0X20001000,
    },

    0x20000400,                      // mem buffer location
    0x20000000,                      // location to write prog_blob in target RAM
    sizeof(flash_code),              // prog_blob size
    flash_code,                      // address of prog_blob
    0x00000400,                      // ram_to_flash_bytes_to_be_written
};
