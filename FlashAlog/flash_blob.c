#include "flash_blob.h"

extern const program_target_t flash_algo_STM32F0XX;
extern const program_target_t flash_algo_STM32F1XX;
extern const program_target_t flash_algo_STM32F4XX;
extern const program_target_t flash_algo_MM32F031X;
extern const program_target_t flash_algo_GD32F10X;
extern const program_target_t flash_algo_MM32F103X;
extern const program_target_t flash_algo_STM32G0XX;
extern const program_target_t flash_algo_STM32L0XX;
extern const program_target_t flash_algo_STM32H7XX;
extern const program_target_t flash_algo_HK32F1XX;


flash_alog_t g_FlashAlog =
{
	0,
	{
		{"STM32F0XX", &flash_algo_STM32F0XX},
		{"STM32G0XX", &flash_algo_STM32G0XX},
		{"STM32L0XX", &flash_algo_STM32L0XX},
		{"STM32F10X", &flash_algo_STM32F1XX},
		{"STM32F4XX", &flash_algo_STM32F4XX},
		{"STM32H7XX", &flash_algo_STM32H7XX},
		{"MM32F031X", &flash_algo_MM32F031X},
		{"MM32F103X", &flash_algo_MM32F103X},
		{"GD32F10X",  &flash_algo_GD32F10X},
		{"HK32F1XX",  &flash_algo_HK32F1XX},
	},
};
